#include <iostream>
#include <functional>
#include <algorithm>
#include <memory>

#include "utils/Options.h"
#include "input/Reactor.h"
#include "input/Event_Handler.h"
#include "factory/Expression_Tree_Factory.h"
#include "iterators/Iterator.h"
#include <memory>

int 
main (int argc, char *argv[])
{
  std::unique_ptr<Expression_Tree_Factory> f(Expression_Tree_Factory::instance());

  // Create Options singleton to parse command line options.
  std::unique_ptr<Options> options (Options::instance ());

  // Parse the command-line options. 
  if (!options->parse_args (argc, argv))
    return 0;

  // Create Reactor singleton to run application event loop.
  std::unique_ptr<Reactor> reactor (Reactor::instance ());

  // Dynamically allocate the appropriate event handler based on the
  // command-line options and register this event handler with the
  // reactor, which is responsible for triggering the deletion of
  // the event handler.
  reactor->register_input_handler
    (Event_Handler::make_handler (options->verbose ()));

  // Run the reactor's event loop, which drives all the processing via
  // callbacks to registered event handlers.
  reactor->run_event_loop ();

  // The std::unique_ptr destructors automatically destroy the
  // singletons.
  return 0;

  /*
  std::unique_ptr<Expression_Tree_Factory> f(Expression_Tree_Factory::instance());
  Expression_Tree tree (new Leaf_Node(1));

  Expression_Tree right (tree.right());

  std::string traversal_order = "in-order";
  Expression_Tree_Iterator it (f->make_iterator(tree, traversal_order, true));
  Visitor vis(f->make_visitor("print_visitor"));
  User_Command command (f->make_command("print"));
  User_Command m_command (f->make_macro_command("print"));
  */
}
