/* -*- C++ -*- */
#include <algorithm>
#include <string>

#include "factory/Expression_Tree_Factory.h"
#include "iterators/Iterator.h"

// Default ctor

Expression_Tree::Expression_Tree ()
  : root_ (nullptr) {    
}

// Ctor take an underlying NODE*.

Expression_Tree::Expression_Tree (Component_Node *root, bool increase_count)
  : root_ (root, increase_count) {    
}

// Copy ctor

Expression_Tree::Expression_Tree (const Expression_Tree &t) = default;

// Assignment operator

Expression_Tree &
Expression_Tree::operator= (const Expression_Tree &t) {
  // Refcounter class takes care of the internal decrements and
  // increments.
  if (this != &t)
    root_ = t.root_;

  return *this;
}

// Dtor

Expression_Tree::~Expression_Tree () = default;

// Check if the tree is empty.

bool
Expression_Tree::is_null () const {
  return root_.get_ptr () == nullptr;
}

// return root pointer

Component_Node *
Expression_Tree::get_root () const {
  return root_.get_ptr ();
}

// Return the stored item.

int 
Expression_Tree::item () const {
  return root_->item ();
}

// Return the left branch.

Expression_Tree
Expression_Tree::left () const {
  return Expression_Tree (root_->left (), true);
  // return Expression_Tree (root_->left ());
}

// Return the right branch.

Expression_Tree
Expression_Tree::right () const {
  // @@ This is causing a memory leak..
  return Expression_Tree (root_->right (), true);
  // return Expression_Tree (root_->right ());
}

// Return a begin iterator of a specified type.

Expression_Tree::iterator
Expression_Tree::begin (const std::string &traversal_order) {
  return Expression_Tree::iterator
    (Expression_Tree_Factory::instance()->make_iterator(*this,
                                                        traversal_order,
                                                        false));
}

// Return an end iterator of a specified type.

Expression_Tree::iterator
Expression_Tree::end (const std::string &traversal_order) {
  return Expression_Tree::iterator 
    (Expression_Tree_Factory::instance()->make_iterator(*this,
                                                        traversal_order,
                                                        true));
}

// Return a begin iterator of a specified type.

Expression_Tree::const_iterator
Expression_Tree::begin (const std::string &traversal_order) const {
  auto *non_const_this = const_cast <Expression_Tree *> (this);
  return Expression_Tree::const_iterator
    (Expression_Tree_Factory::instance()->
     make_iterator(*non_const_this,
                  traversal_order,
                  false));
}

// Return an end iterator of a specified type.

Expression_Tree::const_iterator
Expression_Tree::end (const std::string &traversal_order) const {
    auto *non_const_this = const_cast <Expression_Tree *> (this);
    return Expression_Tree::const_iterator
            (Expression_Tree_Factory::instance()->
                    make_iterator(*non_const_this,
                                  traversal_order,
                                  true));
}

/// Accept a visitor to perform some action on the Expression_Tree.
void
Expression_Tree::accept (Visitor &visitor) const {
  root_->accept (visitor);
}
