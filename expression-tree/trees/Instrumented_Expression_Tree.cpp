
#include "Instrumented_Expression_Tree.h"

/**
     * Ctor that takes a @a Node * that contains all the nodes in the
     * expression tree.
     */
Instrumented_Expression_Tree::Instrumented_Expression_Tree(Component_Node *root, ostream &os)
: super(root), os_(os){}


/**
     * Returns whether a the tree is null.
     */
bool Instrumented_Expression_Tree::isNull() {
    os_<<"starting isNull() call"<<endl;
    bool temp = super::is_null();
    os_<<"finished isNull() call"<<endl;
    return temp;
}

/**
     * Returns root.
     */
Component_Node* Instrumented_Expression_Tree::getRoot() {
    os_<<"starting getRoot() call"<<endl;
    Component_Node *temp = super::get_root();
    os_<<"finished getRoot() call"<<endl;
    return temp;
}

/**
     * Returns the root getItem.
     */
int Instrumented_Expression_Tree::getItem() {
    os_<<"starting mLeft() call"<<endl;
    int temp = super::item();
    os_<<"finished mLeft() call"<<endl;
    return temp;
}

/**
     * Returns the tree's mLeft node.
     */
Expression_Tree Instrumented_Expression_Tree::getLeftChild() {
    os_<<"starting mLeft() call"<<endl;
    Expression_Tree temp = super::left();
    os_<<"finished mRight() call"<<endl;
    return temp;
}

/**
     * Returns the tree's mRight node.
     */
Expression_Tree Instrumented_Expression_Tree::getRightChild() {
    os_<<"starting mRight() call"<<endl;
    Expression_Tree temp = super::right();
    os_<<"finished mRight() call"<<endl;
    return temp;
}

/**
     * Accepts a @a visitor.
     */
void Instrumented_Expression_Tree::accept(Visitor visitor) {
    os_<<"starting accept() call"<<endl;
    super::accept(visitor);
    os_<<"finished accept() call"<<endl;

}


