//
// Created by Sunnie Chang on 2020/9/14.
//

#ifndef EXPRESSION_TREE_INSTRUMENTED_EXPRESSION_TREE_H
#define EXPRESSION_TREE_INSTRUMENTED_EXPRESSION_TREE_H

#include "trees/Expression_Tree.h"
#include "iterators/Iterator_Factory.h"
#include "iterators/Iterator.h"
#include "composites/Component_Node.h"
#include "visitors/Visitor.h"
#include "utils/Refcounter.h"
#include <iterator>
#include <cstdlib>
#include <iostream>
using namespace std;


/**
 * Interface for the Composite pattern that is used to contain all the
 * operator and operand nodes in the expression tree.  Plays the role
 * of the "Abstraction" in the Bridge pattern and delegates to the
 * appropriate "Implementor" that performs the expression tree
 * operations, after first logging the start and finish of each call.
 */
class Instrumented_Expression_Tree : public Expression_Tree{

public:

    typedef Expression_Tree super;

    /**
     * Ctor that takes a @a Node * that contains all the nodes in the
     * expression tree.
     */
     Instrumented_Expression_Tree(Component_Node *root, ostream &os);

    /**
     * Returns whether a the tree is null.
     */
     bool isNull();

    /**
     * Returns root.
     */
     Component_Node* getRoot();

    /**
     * Returns the root getItem.
     */
     int getItem();

    /**
     * Returns the tree's mLeft node.
     */
     Expression_Tree getLeftChild();

    /**
     * Returns the tree's mRight node.
     */
     Expression_Tree getRightChild();

    /**
     * Accepts a @a visitor.
     */
    void accept(Visitor visitor);




private:


    ostream &os_;



};


#endif //EXPRESSION_TREE_INSTRUMENTED_EXPRESSION_TREE_H
