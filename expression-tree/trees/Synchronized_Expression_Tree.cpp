
#include "Synchronized_Expression_Tree.h"

/**
     * Ctor that takes a @a Node * that contains all the nodes in the
     * expression tree.
     */
Synchronized_Expression_Tree::Synchronized_Expression_Tree(Component_Node *root)
:super(root){

}

/**
     * Returns whether a the tree is null.
     */
bool Synchronized_Expression_Tree::isNull() {
    bool temp;
    lock_guard<mutex> guard(mutex_);
    temp = super::is_null();
    return temp;

}

/**
  * Returns root.
  */
Component_Node* Synchronized_Expression_Tree::getRoot() {
    Component_Node* temp;
    lock_guard<mutex> guard(mutex_);
    temp = super::get_root();

    return temp;
}

/**
   * Returns the root getItem.
   */
int Synchronized_Expression_Tree::getItem() {
    int temp;
    lock_guard<mutex> guard(mutex_);
    temp = super::item();
    return temp;
}

/**
  * Returns the tree's mLeft node.
  */
Expression_Tree Synchronized_Expression_Tree::getLeftChild() {
    Expression_Tree temp;
    lock_guard<mutex> guard(mutex_);
    temp = super::left();
    return temp;
}

/**
   * Returns the tree's mRight node.
   */
Expression_Tree Synchronized_Expression_Tree::getRightChild() {
    Expression_Tree temp;
    lock_guard<mutex> guard(mutex_);
    temp = super::right();
    return temp;
}

/**
     * Accepts a @a visitor.
     */
void Synchronized_Expression_Tree::accept(Visitor visitor) {
    lock_guard<mutex> guard(mutex_);
    super::accept(visitor);

}
