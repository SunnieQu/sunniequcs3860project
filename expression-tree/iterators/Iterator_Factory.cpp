#include "Iterator_Factory.h"
#include "Iterator_Impl.h"

Iterator_Factory::Iterator_Factory () {
  traversal_map_["in-order"] = &Iterator_Factory::make_in_order_tree_iterator;
  traversal_map_["pre-order"] = &Iterator_Factory::make_pre_order_tree_iterator;
  traversal_map_["post-order"] = &Iterator_Factory::make_post_order_tree_iterator;
  traversal_map_["level-order"] = &Iterator_Factory::make_level_order_tree_iterator;
}

Iterator_Impl *
Iterator_Factory::make_level_order_tree_iterator (Expression_Tree &tree,
                                                  bool end_iter) { 
  return new Level_Order_Iter_Impl (tree, end_iter);
}

Iterator_Impl *
Iterator_Factory::make_in_order_tree_iterator (Expression_Tree &tree,
                                               bool end_iter) { 
  return new In_Order_Iterator_Impl (tree, end_iter);
}

Iterator_Impl *
Iterator_Factory::make_pre_order_tree_iterator (Expression_Tree &tree,
                                                bool end_iter) { 
  return new Pre_Order_Iterator_Impl (tree, end_iter);
}

Iterator_Impl *
Iterator_Factory::make_post_order_tree_iterator (Expression_Tree &tree,
                                                 bool end_iter) { 
  return new Post_Order_Iterator_Impl (tree, end_iter);
}

Iterator_Impl *
Iterator_Factory::make_iterator (Expression_Tree &tree,
                                 const std::string &traversal_order,
                                 bool end_iter) {  
  auto iter = traversal_map_.find (traversal_order);
  if (iter == traversal_map_.end ())
    // We don't understand the type. Convert the type to a string
    // and pass it back via an exception

    throw Iterator_Factory::Invalid_Iterator (traversal_order);
  else
    return (*iter->second) (tree, end_iter);
}
