#ifndef _ITERATOR_FACTORY_H
#define _ITERATOR_FACTORY_H

#include <string>
#include <map>
#include <stdexcept>

class Iterator_Impl;
class Expression_Tree;

/**
 * @class Iterator_Factory
 *
 * @brief Implementation of a factory pattern that dynamically
 *        allocates the appropriate @a Iterator_Impl object.
 * 
 *        This is a variant of the Abstract Factory pattern that has a
 *        set of related factory methods but which doesn't use
 *        inheritance.
 *
 * @see Level_Order_Iterator_Impl, In_Order_Iterator_Impl,
 *        Pre_Order_Iterator_Impl, and Post_Order_Iterator_Impl.
 */
class Iterator_Factory {
public:
  /// Exception class.
  class Invalid_Iterator : public std::domain_error {
  public:
      explicit Invalid_Iterator(const std::string &message)
                : std::domain_error(message) {}
  };

  /// Constructor.
  Iterator_Factory ();

  /// Dynamically allocate a new @a Iterator_Impl object based on the
  /// designated @a traversal_order and @a end_iter.
  Iterator_Impl *make_iterator (Expression_Tree &tree,
                                const std::string &traversal_order,
                                bool end_iter);

private:
  /// Dynamically allocate a new @a Level_Order_Iterator_Impl object
  /// based on the designated @a end_iter.
  static Iterator_Impl *make_in_order_tree_iterator (Expression_Tree &tree,
                                                     bool end_iter);

  /// Dynamically allocate a new @a Pre_Order_Iterator_Impl object
  /// based on the designated @a end_iter.
  static Iterator_Impl *make_pre_order_tree_iterator (Expression_Tree &tree,
                                                      bool end_iter);

  /// Dynamically allocate a new @a Post_Order_Iterator_Impl object
  /// based on the designated @a end_iter.
  static Iterator_Impl *make_post_order_tree_iterator (Expression_Tree &tree,
                                                       bool end_iter);

  /// Dynamically allocate a new @a Level_Order_Iterator_Impl object
  /// based on the designated @a end_iter.
  static Iterator_Impl *make_level_order_tree_iterator (Expression_Tree &tree,
                                                        bool end_iter);

  typedef Iterator_Impl *(*TRAVERSAL_PTMF) (Expression_Tree &tree, bool end_iter);
  typedef std::map <std::string, TRAVERSAL_PTMF> TRAVERSAL_MAP;

  TRAVERSAL_MAP traversal_map_;
};

#endif // _ITERATOR_FACTORY_H
