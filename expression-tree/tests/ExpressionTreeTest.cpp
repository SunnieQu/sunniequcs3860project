//
// Created by Sunnie Chang on 2020/11/11.
//

#include <iostream>
//include the google test dependencies
#include <gtest/gtest.h>
#include <iterators/Iterator.h>
#include "trees/Expression_Tree.h"
#include "composites/Leaf_Node.h"
#include "composites/Composite_Add_Node.h"
#include "composites/Composite_Binary_Node.h"
#include "composites/Composite_Subtract_Node.h"
#include "composites/Composite_Negate_Node.h"
#include "composites/Composite_Multiply_Node.h"
#include "visitors/Visitor.h"
#include "factory/Expression_Tree_Factory.h"



namespace {
// The fixture for testing Expression_Tree class.
    class ExpressionTreeTest : public ::testing::Test {
    };

    TEST_F(ExpressionTreeTest, Constructor) {

        EXPECT_NO_THROW( Expression_Tree ET; );

        EXPECT_NO_THROW(
                           Expression_Tree ET2(new Leaf_Node(1)););

    }

    TEST_F(ExpressionTreeTest, Add){
        Component_Node *left = new Leaf_Node(2);
        Component_Node *right = new Leaf_Node(3);
        Expression_Tree ET(new Composite_Add_Node(left,right));
        EXPECT_EQ(ET.left(), 2);
        EXPECT_EQ(ET.right(), 3);
        EXPECT_EQ(ET.item(), '+');
    }

    TEST_F(ExpressionTreeTest, Subtract){
        Component_Node *left = new Leaf_Node(2);
        Component_Node *right = new Leaf_Node(3);
        Expression_Tree ET(new Composite_Subtract_Node(left,right));
        EXPECT_EQ(ET.left(), 2);
        EXPECT_EQ(ET.right(), 3);
        EXPECT_EQ(ET.item(), '-');
    }

    TEST_F(ExpressionTreeTest, Negate){
        Component_Node *right = new Leaf_Node(1);
        Expression_Tree ET (new Composite_Negate_Node(right));
        EXPECT_EQ(ET.right(), 3);
        EXPECT_EQ(ET.item(), '-');
    }

    TEST_F(ExpressionTreeTest, Multiply){
        Component_Node *left = new Leaf_Node(2);
        Component_Node *right = new Leaf_Node(3);
        Expression_Tree ET(new Composite_Multiply_Node(left,right));
        EXPECT_EQ(ET.left(), 2);
        EXPECT_EQ(ET.right(), 3);
        EXPECT_EQ(ET.item(), '*');
    }

    TEST_F(ExpressionTreeTest, Divide){
        Component_Node *left = new Leaf_Node(2);
        Component_Node *right = new Leaf_Node(3);
        Expression_Tree ET(new Composite_Multiply_Node(left,right));
        EXPECT_EQ(ET.left(), 2);
        EXPECT_EQ(ET.right(), 3);
        EXPECT_EQ(ET.item(), '/');
    }

    TEST_F(ExpressionTreeTest, Leaf){
        Leaf_Node *leaf = new Leaf_Node(2);
        EXPECT_EQ(leaf->item(), 2);
    }

    TEST_F(ExpressionTreeTest, Visitor){
        Component_Node *left = new Leaf_Node(2);
        Component_Node *right = new Leaf_Node(3);
        Expression_Tree ET(new Composite_Add_Node(left,right));
        std::unique_ptr<Expression_Tree_Factory> f(Expression_Tree_Factory::instance());
        std::string traversal_order = "in-order";

        Visitor vis(f->make_visitor("evaluation_visitor"));
        for(Expression_Tree_Iterator it (f->make_iterator(ET, traversal_order, true));
        it != ET.end(traversal_order); ++it){
            it->accept(vis);
        }
        EXPECT_EQ(vis.total(),5);
    }








}
