#ifndef _VISITOR_FACTORY_H
#define _VISITOR_FACTORY_H

#include "Visitor_Factory.h"
#include "Visitor_Factory_Impl.h"

// Default constructor.
Visitor_Factory::Visitor_Factory()
  : visitor_factory_impl_ (new Visitor_Factory_Impl ()) {
}

// Copy ctor - needed for reference counting.
Visitor_Factory::Visitor_Factory(const Visitor_Factory &f)
  // wrap the pointer in a ref count object
  : visitor_factory_impl_ (f.visitor_factory_impl_){

}

/// Assignment operator - needed for reference counting.
Visitor_Factory &Visitor_Factory::operator=(const Visitor_Factory &f) {
  // check for self assignment first
  if (this != &f)
    /// we just make use of the Refcounter functionality here
    visitor_factory_impl_ = f.visitor_factory_impl_;

  return *this;

}

// Destructor.
Visitor_Factory::~Visitor_Factory() = default;

// Method to make the visitor
Visitor Visitor_Factory::make_visitor(const std::string &s) {
  return visitor_factory_impl_->make_visitor (s);
}

#endif // _VISITOR_FACTORY_H



