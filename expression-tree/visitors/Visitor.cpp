#include "visitors/Visitor.h"
#include "visitors/Visitor_Impl.h"

Visitor::Visitor(Visitor_Impl *impl)
  : visitor_(impl) {
}

Visitor::~Visitor() = default;

void Visitor::visit(const Leaf_Node &node) {
    return visitor_->visit(node);
}

void Visitor::visit(const Composite_Negate_Node &node) {
    return visitor_->visit(node);
}

void Visitor::visit(const Composite_Add_Node &node) {
    return visitor_->visit(node);
}

void Visitor::visit(const Composite_Subtract_Node &node) {
    return visitor_->visit(node);
}

void Visitor::visit(const Composite_Divide_Node &node) {
    return visitor_->visit(node);
}

void Visitor::visit(const Composite_Multiply_Node &node) {
    return visitor_->visit(node);
}

int Visitor::total() {
    return visitor_->total();
}

