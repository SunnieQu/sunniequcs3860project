#ifndef EXPRESSION_TREE_VISITOR_H
#define EXPRESSION_TREE_VISITOR_H

#include "utils/Refcounter.h"

// forward declarations
class Visitor_Impl;
class Leaf_Node;
class Composite_Negate_Node;
class Composite_Add_Node;
class Composite_Subtract_Node;
class Composite_Divide_Node;
class Composite_Multiply_Node;

/**
 * @class Visitor
 * @brief Abstract base class for all visitors to all classes that derive
 *        from @a Component_Node.
 *
 *        This class plays the role of the "visitor" in the Visitor
 *        pattern. Plays the role of the "abstraction" class in the
 *        Bridge pattern and delegates to the appropriate
 *        "implementor" that performs the visitor operations.
 */
class Visitor {
public:
  /// Constructor.
  explicit Visitor (Visitor_Impl *impl);

  /// destructor
  virtual ~Visitor ();

  /// Visit a @a Leaf_Node.
  void visit (const Leaf_Node &node);

  /// Visit a @a Composite_Negate_Node.
  void visit (const Composite_Negate_Node &node);

  /// Visit a @a Composite_Add_Node.
  void visit (const Composite_Add_Node &node);

  /// Visit a @a Composite_Subtract_Node.
  void visit (const Composite_Subtract_Node &node);

  /// Visit a @a Composite_Divide_Node.
  void visit (const Composite_Divide_Node &node);

  /// Visit a @a Composite_Multiply_Node.
  void visit (const Composite_Multiply_Node &node);

  /// Print the total of the evaluation.
  int total ();

private:
  /// Pointer to actual implementation, i.e., the "bridge", which is
  /// reference counted to automate memory management.
  Refcounter<Visitor_Impl> visitor_;
};


#endif // EXPRESSION_TREE_VISITOR_H
