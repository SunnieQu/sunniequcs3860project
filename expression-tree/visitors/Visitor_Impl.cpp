#include "visitors/Visitor_Impl.h"

#include <iostream>

/// print a total for the evaluation
int Visitor_Impl::total() {
    return 0;
}

///Destructor for Evaluation_Visitor Class
Evaluation_Visitor::~Evaluation_Visitor() = default;

/// base evaluation for a node. This is used by Leaf_Node
void Evaluation_Visitor::visit(const Leaf_Node &node) {
    stack_.push (node.item ());
}
/// evaluation of a negation (Composite_Negate_Node)
void Evaluation_Visitor::visit(const Composite_Negate_Node &node) {
  if (!stack_.empty()) {
    auto arg = stack_.top();
    stack_.pop();
    stack_.push(-arg);
  }
}

/// evaluation of an addition (Composite_Add_Node)
void Evaluation_Visitor::visit(const Composite_Add_Node &node) {
  if (stack_.size () >= 2) {
    auto rhs = stack_.top();
    stack_.pop();
    auto lhs = stack_.top();
    stack_.pop();
    stack_.push(lhs + rhs);
  }
  // std::cout << "add current top: " << stack_.top () << std::endl;
}

/// evaluation of an addition (Composite_Subtract_Node)
void Evaluation_Visitor::visit(const Composite_Subtract_Node &node) {
  if (stack_.size () >= 2) {
    auto rhs = stack_.top ();
    stack_.pop ();
    auto lhs = stack_.top ();
    stack_.pop ();
    stack_.push (lhs - rhs);
  }
}

/// evaluations of a division (Composite_Divide_Node)
void Evaluation_Visitor::visit(const Composite_Divide_Node &node) {
  if (stack_.size () >= 2 && stack_.top ()) {
    auto rhs = stack_.top ();
    stack_.pop ();
    auto lhs = stack_.top ();
    stack_.pop ();
    stack_.push (lhs / rhs );
  } else {
    std::cout << "\n\n**ERROR**: Division by zero is not allowed. ";
    std::cout << "Resetting evaluation visitor.\n\n";
    reset ();
  }
}

/// evaluations of a division (Composite_Multiply_Node)
void Evaluation_Visitor::visit(const Composite_Multiply_Node &node) {
  if (stack_.size () >= 2) {
    auto rhs = stack_.top ();
    stack_.pop ();
    auto lhs = stack_.top ();
    stack_.pop ();
    stack_.push(lhs * rhs);
  }
}

/// print a total for the evaluation
int Evaluation_Visitor::total() {
  if (!stack_.empty ())
    return stack_.top ();
  else
    return 0;
}

/// reset the evaluation
void Evaluation_Visitor::reset() {
  // Reset the stack to 0.
  stack_ = std::stack<int>();
}

/// Desctructor for Print_Visitor Class
Print_Visitor::~Print_Visitor() = default;

/// visit function - prints Leaf_Negate_Node contents to std::cout
void Print_Visitor::visit(const Leaf_Node &node) {
  std::cout << " " << node.item ();
}

/// visit function - prints Composite_Negate_Node contents to std::cout
void Print_Visitor::visit(const Composite_Negate_Node &node) {
  std::cout.put ('-');
}

/// visit function - prints Composite_Add_Node contents to std::cout
void Print_Visitor::visit(const Composite_Add_Node &node) {
  std::cout << "+ ";
}

/// visit function - prints Composite_Subtract_Node contents to std::cout
void Print_Visitor::visit(const Composite_Subtract_Node &node) {
  std::cout << "- ";
}

/// visit function - prints Composite_Divide_Node contents to std::cout
void Print_Visitor::visit(const Composite_Divide_Node &node) {
  std::cout << "/ ";
}

/// visit function - prints Composite_Multiply_Node contents to std::cout
void Print_Visitor::visit(const Composite_Multiply_Node &node) {
  std::cout << "* ";
}
