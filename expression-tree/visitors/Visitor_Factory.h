#ifndef VISITOR_FACTORY_H
#define VISITOR_FACTORY_H

#include <string>

#include "visitors/Visitor.h"
#include "Visitor_Factory_Impl.h"
#include "utils/Refcounter.h"
#include <iostream>
#include "composites/Leaf_Node.h"
#include "composites/Composite_Negate_Node.h"
#include "composites/Composite_Add_Node.h"
#include "composites/Composite_Subtract_Node.h"
#include "composites/Composite_Divide_Node.h"
#include "composites/Composite_Multiply_Node.h"

class Component_Node;
class Composite_Negate_Node;
class Composite_Add_Node;
class Composite_Subtract_Node;
class Composite_Divide_Node;
class Composite_Multiply_Node;

class Iterator_Impl;

/**
 * @class Visitor_Factory
 * @brief Interface for the Factory Method pattern used create the
 *        appropriate @a Visitor based on a string supplied by the
 *        caller.
 *
 *        Plays the role of the "abstraction" class in the Bridge
 *        pattern and delegates to the appropriate "implementor" that
 *        creates the designated commands.
 */
class Visitor_Factory {
public:
  /// Default ctor
  explicit Visitor_Factory ();

  /// Copy ctor - needed for reference counting.
  Visitor_Factory (const Visitor_Factory &f);

  /// Assignment operator - needed for reference counting.
  Visitor_Factory &operator= (const Visitor_Factory &f);

  /// Dtor.
  ~Visitor_Factory ();

  /// Factory method that makes the requested visitor.  This method is
  /// the primary method used by clients.
  Visitor make_visitor (const std::string &visitor);

private:
  /// Pointer to actual implementation, i.e., the "bridge", which is
  /// reference counted to automate memory management.
  Refcounter <Visitor_Factory_Impl> visitor_factory_impl_;
};

#endif // _VISITOR_FACTORY_H
