#ifndef _VISITOR_FACTORY_IMPL_H
#define _VISITOR_FACTORY_IMPL_H

#include <stdexcept>

#include "Visitor_Factory_Impl.h"
#include "Visitor_Impl.h"

// Constructor.
Visitor_Factory_Impl::Visitor_Factory_Impl() : visitor_map_ () {
  visitor_map_["evaluation_visitor"] = &Visitor_Factory_Impl::make_evaluation_visitor;
  visitor_map_["print_visitor"] = &Visitor_Factory_Impl::make_print_visitor;
}

// Destructor
Visitor_Factory_Impl::~Visitor_Factory_Impl() = default;

Visitor Visitor_Factory_Impl::make_visitor(const std::string &input) {
  // Find the PTMF corresponding to the visitor_keyword.
  auto iter = visitor_map_.find (input);

  if (iter != visitor_map_.end ())
    // Invoke the PTMF if it's found in the map.
    return (this->*iter->second)();
  else
    // Quit.
    throw Expression_Tree::Invalid_Iterator (input);
}

Visitor Visitor_Factory_Impl::make_evaluation_visitor() {
  return Visitor (new Evaluation_Visitor());
}

Visitor Visitor_Factory_Impl::make_print_visitor() {
  return Visitor (new Print_Visitor());
}

#endif // _VISITOR_FACTORY_IMPL_H
