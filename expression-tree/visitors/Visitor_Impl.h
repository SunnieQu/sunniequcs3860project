#ifndef EXPRESSION_TREE_VISITOR_IMPL_H
#define EXPRESSION_TREE_VISITOR_IMPL_H

#include <stack>
#include <iostream>
#include "composites/Leaf_Node.h"
#include "composites/Composite_Negate_Node.h"
#include "composites/Composite_Add_Node.h"
#include "composites/Composite_Subtract_Node.h"
#include "composites/Composite_Divide_Node.h"
#include "composites/Composite_Multiply_Node.h"

class Component_Node;
class Composite_Negate_Node;
class Composite_Add_Node;
class Composite_Subtract_Node;
class Composite_Divide_Node;
class Composite_Multiply_Node;

/**
 * @class Visitor_Impl
 * @brief Implementation of the Visitor pattern that is used to define
 *        the various visitor algorithms that can be performed to
 *        perform operations on the expression tree.
 * 
 *        Plays the role of the "implementor" base class in the Bridge
 *        pattern that is used as the basis for the subclasses that
 *        actually define the various visitor algorithms.
 */
class Visitor_Impl {
public:
    /// Construct an Visitor_Impl.
    Visitor_Impl () = default;

    /// Dtor.
    virtual ~Visitor_Impl () = default;

    /// Visit a @a Leaf_Node.
    virtual void visit (const Leaf_Node &node) = 0;

    /// Visit a @a Composite_Negate_Node.
    virtual void visit (const Composite_Negate_Node &node) = 0;

    /// Visit a @a Composite_Add_Node.
    virtual void visit (const Composite_Add_Node &node) = 0;

    /// Visit a @a Composite_Subtract_Node.
    virtual void visit (const Composite_Subtract_Node &node) = 0;

    /// Visit a @a Composite_Divide_Node.
    virtual void visit (const Composite_Divide_Node &node) = 0;

    /// Visit a @a Composite_Multiply_Node.
    virtual void visit (const Composite_Multiply_Node &node) = 0;

    /// Print the total.
    virtual int total();
};

/**
 * @class Evaluation_Visitor
 * @brief This plays the role of a visitor for evaluating nodes in an
 *        expression tree that is being iterated in post-order fashion
 *        (and does not work correctly with any other iterator).
 */
class Evaluation_Visitor: public Visitor_Impl {
public:
  /// Construct an Visitor_Impl for a node
  Evaluation_Visitor () = default;

  /// Dtor.
  ~Evaluation_Visitor () override;

  /// Visit a @a Leaf_Node.
  void visit (const Leaf_Node &node) override;

  /// Visit a @a Composite_Negate_Node.
  void visit (const Composite_Negate_Node &node) override;

  /// Visit a @a Composite_Add_Node.
  void visit (const Composite_Add_Node &node) override;

  /// Visit a @a Composite_Subtract_Node.
  void visit (const Composite_Subtract_Node &node) override;

  /// Visit a @a Composite_Divide_Node.
  void visit (const Composite_Divide_Node &node) override;

  /// Visit a @a Composite_Multiply_Node.
  void visit (const Composite_Multiply_Node &node) override;

  /// Print the total of the evaluation.
  int total () override;

  /// Resets the evaluation to it can be reused.
  void reset ();

private:
  /// Stack used for temporarily storing evaluations.
  std::stack<int> stack_;
};

/**
 * @class Print_Visitor
 * @brief This plays the role of a visitor for printing nodes in an
 *        expression tree that is being iterated in any traversal
 *        order.
 */
class Print_Visitor: public Visitor_Impl{
public:
    /// Construct an Visitor_Impl for a node
    Print_Visitor() = default;

    /// Dtor.
    ~Print_Visitor () override;

    /// Visits a @a Leaf_Node and prints it contents to @a std::cout.
    void visit (const Leaf_Node &node) override;

    /// Visit a @a Composite_Negate_Node and prints its contents to @a
    /// std::cout.
    void visit (const Composite_Negate_Node &node) override;

    /// Visit a @a Composite_Add_Node and prints its contents to @a
    /// std::cout.
    void visit (const Composite_Add_Node &node) override;

    /// Visit a @a Composite_Subtract_Node and prints its contents to
    /// @a std::cout.
    void visit (const Composite_Subtract_Node &node) override;

    /// Visit a @a Composite_Divide_Node and prints its contents to @a
    /// std::cout.
    void visit (const Composite_Divide_Node &node) override;

    /// visit function - prints Composite_Multiply_Node contents to
    /// std::cout
    void visit (const Composite_Multiply_Node &node) override;
};

#endif // EXPRESSION_TREE_VISITOR_IMPL_H

