#ifndef EXPRESSION_TREE_VISITOR_FACTORY_IMPL_H
#define EXPRESSION_TREE_VISITOR_FACTORY_IMPL_H

#include <string>
#include <map>
#include "visitors/Visitor.h"
#include "states/Tree_Context.h"
#include "utils/Refcounter.h"

#include <iostream>
#include "composites/Leaf_Node.h"
#include "composites/Composite_Negate_Node.h"
#include "composites/Composite_Add_Node.h"
#include "composites/Composite_Subtract_Node.h"
#include "composites/Composite_Divide_Node.h"
#include "composites/Composite_Multiply_Node.h"
#include "Visitor_Impl.h"

class Component_Node;
class Composite_Negate_Node;
class Composite_Add_Node;
class Composite_Subtract_Node;
class Composite_Divide_Node;
class Composite_Multiply_Node;

// Forward declarations.

class Iterator_Impl;

/**
 * @class Visitor_Factory_Impl
 * @brief Implementation of the Factory Method pattern that to create
 *        various types of @a Expression_Tree_Visitor.
 *
 *        Plays the role of the "implementor" base class in the Bridge
 *        pattern.
 */
class Visitor_Factory_Impl {
  /// Needed for reference counting.
  friend class Expression_Tree_Visitor_Factory;

public:
  /// Constructor initializes the data member.
  explicit Visitor_Factory_Impl ();

  /// Dtor - make it virtual since we subclass from this interface.
  virtual ~Visitor_Factory_Impl ();

  /// Factory method that makes the requested visitor.  This method is
  /// the primary method used by clients.
  virtual Visitor make_visitor (const std::string &s);

  /// Factory method that make the requested evaluation visitor.
  virtual Visitor make_evaluation_visitor ();

  /// Make the requested print visitor.
  virtual Visitor make_print_visitor ();

private:
  /// Useful typedefs to simplify use of the STL @a std::map.
  typedef Visitor (Visitor_Factory_Impl::*FACTORY_PTMF)();

  // Maps string-based command names to the corresponding
  // pointer-to-member-function.
  typedef std::map<std::string, FACTORY_PTMF> VISITOR_MAP;

  /// Map used to validate user command input and dispatch
  /// corresponding factory method.
  VISITOR_MAP visitor_map_;
};

#endif //EXPRESSION_TREE_VISITOR_FACTORY_IMPL_H
