#ifndef EXPRESSION_TREE_EXPRESSION_TREE_FACTORY_H
#define EXPRESSION_TREE_EXPRESSION_TREE_FACTORY_H

#include <string>
#include "visitors/Visitor_Factory.h"
#include "iterators/Iterator_Factory.h"
#include "commands/User_Command_Factory.h"
#include "trees/Expression_Tree.h"
#include "states/Uninitialized_State_Factory.h"

class Tree_Context;
class Expression_Tree;

/**
 * @class Expression_Tree
 * @brief Abstract Factory Class that groups all factory methods together
 *
 *
 *        This class plays the role of the abstract factory pattern.
 *        Plays the role of the "abstraction" class in the
 *        Bridge pattern and delegates to the appropriate
 *        "implementor/instance" that performs the operation to make the corresponding
 *        factory method.
 */


class Expression_Tree_Factory {
public:
    /// Method to return the one and only instance.
    static Expression_Tree_Factory *instance ();

    /// Destructor
    ~Expression_Tree_Factory();

    /// Disallow copying and assignment since it's a singleton!
    void operator=(const Expression_Tree_Factory &) = delete;
    Expression_Tree_Factory(const Expression_Tree_Factory &) = delete;

    /// Method to make the appropriate Visitor using the bridge pattern
    Visitor make_visitor(const std::string &visitor);

    /// Method to make the appropriate User Command using the bridge pattern
    User_Command make_command(const std::string &command);

    /// Method to make the appropriate Macro User Command using the bridge pattern
    User_Command make_macro_command(const std::string &command);

    /// Method to make the appropriate Iterator using the bridge pattern
    Iterator_Impl *make_iterator(Expression_Tree &tree,
                                 const std::string &traversal_order,
                                 bool end_iter);

    /// Method to make the appropriate Uninitialized State using the bridge pattern
    State *make_uninitialized_state(const std::string &format);

private:
    /// Make the constructor private for a singleton.
    Expression_Tree_Factory();

    /// Pointer to the singleton @a Options instance.
    static Expression_Tree_Factory *instance_;

    /// An instance of the visitor factory for making the visitor
    Visitor_Factory visitor_factory_;

    /// An instance of the tree context to initialize the user command factory instance
    Tree_Context tree_context_;

    /// An instance of the user command factory for making the user command and the macro command
    User_Command_Factory user_command_factory_;

    /// An instance of the uninitialized state factory for making the uninitialized state
    Uninitialized_State_Factory uninitialized_state_factory_;

    /// An instance of the iterator factory for making the iterator
    Iterator_Factory iterator_factory_;
};

#endif //EXPRESSION_TREE_EXPRESSION_TREE_FACTORY_H
