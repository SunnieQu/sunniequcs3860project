
#include "Expression_Tree_Factory.h"

Expression_Tree_Factory *
Expression_Tree_Factory::instance_;

/// Method to return the one and only instance.
Expression_Tree_Factory *Expression_Tree_Factory::instance() {
    // Create the options implementation if it hasn't already been done
    if (instance_ == nullptr) {
        instance_ = new Expression_Tree_Factory();
    }

    return instance_;
}

/// Destructor
Expression_Tree_Factory::~Expression_Tree_Factory() = default;

/// Method to make the appropriate Visitor using the bridge pattern
Visitor
Expression_Tree_Factory::make_visitor(const std::string &visitor) {
    return visitor_factory_.make_visitor(visitor);
}

/// Method to make the appropriate User Command using the bridge pattern
User_Command
Expression_Tree_Factory::make_command(const std::string &command) {
    return user_command_factory_.make_command(command);
}

/// Method to make the appropriate Macro User Command using the bridge pattern
User_Command
Expression_Tree_Factory::make_macro_command(const std::string &command) {
    return user_command_factory_.make_macro_command(command);
}

/// Method to make the appropriate Iterator using the bridge pattern
Iterator_Impl *
Expression_Tree_Factory::make_iterator(Expression_Tree &tree,
                                            const std::string &traversal_order,
                                            bool end_iter) {
  return iterator_factory_.make_iterator(tree, traversal_order, end_iter);
}

/// Method to make the appropriate Uninitialized State using the bridge pattern
State *
Expression_Tree_Factory::make_uninitialized_state(const std::string &format) {
  return uninitialized_state_factory_.make_uninitialized_state(format);
}

/// Make the constructor private for a singleton.
Expression_Tree_Factory::Expression_Tree_Factory()
        : user_command_factory_(tree_context_) {
}




