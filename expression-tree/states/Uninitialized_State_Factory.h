#ifndef _UNINITIALIZED_STATE_FACTORY_H
#define _UNINITIALIZED_STATE_FACTORY_H

#include <string>
#include <map>
#include <stdexcept>

class State;

/**
 * @class Uninitialized_State_Factory
 *
 * @brief Implementation of a factory pattern that dynamically
 *        allocates the appropriate @a State object.
 *
 *        This is a variant of the Abstract Factory pattern that has
 *        a set of related factory methods but which doesn't use
 *        inheritance.
 */
class Uninitialized_State_Factory {
public:
  /// Exception class for Invalid States exceptions
  class Invalid_Iterator : public std::domain_error {
  public:
      explicit Invalid_Iterator(const std::string &message)
              : std::domain_error(message) {}
  };

    /// Constructor.
  Uninitialized_State_Factory ();

  /// Dynamically allocate a new @a State object based on the
  /// designated @a traversal_order and @a end_iter.
  State *make_uninitialized_state (const std::string &format);

private:
  /// Dynamically allocate a new @a Level_Order_Uninitialized_State
  /// object based on the designated @a end_iter.
  static State *make_in_order_uninitialized_state ();

  /// Dynamically allocate a new @a Pre_Order_Uninitialized_State
  /// object based on the designated @a end_iter.
  static State *make_pre_order_uninitialized_state ();

  /// Dynamically allocate a new @a Post_Order_Uninitialized_State
  /// object based on the designated @a end_iter.
  static State *make_post_order_uninitialized_state ();

  /// Dynamically allocate a new @a Level_Order_Uninitialized_State
  /// object based on the designated @a end_iter.
  static State *make_level_order_uninitialized_state ();

  typedef State *(*UNINIT_STATE_PTF) ();
  typedef std::map <std::string, UNINIT_STATE_PTF> UNINITIALIZED_STATE_MAP;

  static UNINITIALIZED_STATE_MAP uninit_state_map_;
};

#endif /* _UNINITIALIZED_STATE_FACTORY_H */
