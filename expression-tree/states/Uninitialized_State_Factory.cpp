#include "Uninitialized_State_Factory.h"
#include "State.h"

// Static data member definitions.
Uninitialized_State_Factory::UNINITIALIZED_STATE_MAP
Uninitialized_State_Factory::uninit_state_map_;

Uninitialized_State_Factory::Uninitialized_State_Factory () {
  uninit_state_map_["in-order"]
    = &Uninitialized_State_Factory::make_in_order_uninitialized_state;
  uninit_state_map_["pre-order"]
    = &Uninitialized_State_Factory::make_pre_order_uninitialized_state;
  uninit_state_map_["post-order"]
    = &Uninitialized_State_Factory::make_post_order_uninitialized_state;
  uninit_state_map_["level-order"]
    = &Uninitialized_State_Factory::make_level_order_uninitialized_state;
}

State *
Uninitialized_State_Factory::make_level_order_uninitialized_state () {
  return new Level_Order_Uninitialized_State ();
}

State *
Uninitialized_State_Factory::make_in_order_uninitialized_state () {
  return new In_Order_Uninitialized_State ();
}

State *
Uninitialized_State_Factory::make_pre_order_uninitialized_state () {
  return new Pre_Order_Uninitialized_State ();
}

State *
Uninitialized_State_Factory::make_post_order_uninitialized_state () {
  return new Post_Order_Uninitialized_State ();
}

State *
Uninitialized_State_Factory::make_uninitialized_state (const std::string &format) {
  auto iter = uninit_state_map_.find (format);

  if (iter == uninit_state_map_.end ())
    // We don't understand the type. Convert the type to a string and
    // pass it back via an exception.
    throw Uninitialized_State_Factory::Invalid_Iterator (format);
  else
    return (*iter->second) ();
}           

