#include <stdexcept>
#include <iostream>
#include <algorithm>

#include "Tree_Context.h"
#include "iterators/Iterator.h"
#include "visitors/Visitor_Factory.h"
#include "factory/Expression_Tree_Factory.h"

/// this method traverses the tree in with a given traversal strategy
void 
State::print_tree (const Expression_Tree &tree,
                   const std::string &traversal_order,
                   std::ostream &os) {
  os << "traverse tree using strategy '" << traversal_order
     << "':" << std::endl;

  // Create a print visitor.
  Visitor print_visitor = Expression_Tree_Factory::instance()
          ->make_visitor("print_visitor");

  // std::for_each() is a short-hand for writing the following loop:
  for (auto iter = tree.begin (traversal_order);
       iter != tree.end (traversal_order);
       ++iter)
    iter->accept (print_visitor);

#if 0
  std::for_each (tree.begin (traversal_order),
                 tree.end (traversal_order),
                 [&print_visitor] (auto tree)
                 {
                   tree.accept (print_visitor);
                 });
#endif

  os << std::endl;
}

void 
State::evaluate_tree (const Expression_Tree &tree,
                      const std::string &traversal_order,
                      std::ostream &os) {
  // os << "Evaluating tree using strategy '" << traversal_order
  // << "':" << std::endl;

  Visitor evaluation_visitor = Expression_Tree_Factory::instance()
            ->make_visitor("evaluation_visitor");

  std::for_each (tree.begin (traversal_order),
                 tree.end (traversal_order),
                 [&evaluation_visitor](const Expression_Tree &t) {
                   t.accept(evaluation_visitor);
                 });

#if 0
  // std::for_each() is a short-hand for writing the following loop:
  for (auto iter = tree.begin (traversal_order);
       iter != tree.end (traversal_order);
       ++iter)
    iter->accept (evaluation_visitor);
#endif
  std::cout << evaluation_visitor.total() << std::endl;
}

void 
State::format (Tree_Context &,
               const std::string &) {
  throw State::Invalid_State ("State::format called in invalid state");
}           

void 
State::expr (Tree_Context &,
             const std::string &) {
  throw State::Invalid_State ("State::expr called in invalid state");
}                

void 
State::print (Tree_Context &,
              const std::string &) {
  throw State::Invalid_State ("State::print called in invalid state");
}

void 
State::evaluate (Tree_Context &,
                 const std::string &) {
  throw State::Invalid_State ("State::evaluate called in invalid state");
}

void 
Uninitialized_State::format (Tree_Context &context,
                             const std::string &new_format) {
  // Call factory method to initialize the context state.
  context.state (Expression_Tree_Factory::instance()->make_uninitialized_state
                 (new_format));
}

void 
Post_Order_Uninitialized_State::expr (Tree_Context &tree_context,
                                      const std::string &expr) {
  Interpreter interpreter
    (new Post_Order_Interpreter
     (tree_context.interpreter_context_));

  tree_context.tree (interpreter.interpret (expr));

  tree_context.state (new Post_Order_Initialized_State);
}                

void 
Post_Order_Initialized_State::print (Tree_Context &context,
                                     const std::string &format) {
  State::print_tree (context.tree (), format, std::cout);
}

void 
Post_Order_Initialized_State::evaluate (Tree_Context &context,
                                        const std::string &param) {
  State::evaluate_tree (context.tree (), param, std::cout);
}

void 
In_Order_Uninitialized_State::expr (Tree_Context &tree_context,
                                    const std::string &expr) {
  Interpreter interpreter
    (new In_Order_Interpreter
       (tree_context.interpreter_context_));

  tree_context.tree (interpreter.interpret (expr));

  tree_context.state (new In_Order_Initialized_State);
}                

void 
In_Order_Initialized_State::print (Tree_Context &context,
                                   const std::string &format) {
  State::print_tree (context.tree (), format, std::cout);
  
}

void 
In_Order_Initialized_State::evaluate (Tree_Context &context,
                                      const std::string &param) {
  State::evaluate_tree (context.tree (), param, std::cout);
}


void
Pre_Order_Uninitialized_State::expr (Tree_Context &context,
                                     const std::string &expression) {
}

void
Pre_Order_Initialized_State::print (Tree_Context &context,
                                    const std::string &format) {
    State::print_tree (context.tree (), format, std::cout);
}

void
Pre_Order_Initialized_State::evaluate (Tree_Context &context,
                                       const std::string &param) {
    State::evaluate_tree (context.tree (), param, std::cout);
}


void
Level_Order_Uninitialized_State::expr (Tree_Context &tree_context,
                                       const std::string &expr) {
}

void
Level_Order_Initialized_State::print (Tree_Context &context,
                                      const std::string &format) {
    State::print_tree (context.tree (), format, std::cout);
}

void
Level_Order_Initialized_State::evaluate (Tree_Context &context,
                                         const std::string &param) {
    State::evaluate_tree (context.tree (), param, std::cout);
}
