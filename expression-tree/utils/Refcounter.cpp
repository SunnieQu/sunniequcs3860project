/* -*- C++ -*- */
#ifndef _REFCOUNTER_CPP_
#define _REFCOUNTER_CPP_

#include "Refcounter.h"
#include <iostream>

/// default Ctor
template <typename T>
Refcounter<T>::Refcounter ()
  : ptr_ (0) {
  // std::cout<<"Refcounter::Refcounter(): "<<this<<" "<<ptr_->refcount_<<std::endl;
}

/// Ctor with refcounting functionality
template <typename T>
Refcounter<T>::Refcounter (T *ptr, bool omit_delete)
  : ptr_ (new Shim (ptr, omit_delete)) {
}

  /// copy Ctor
template <typename T>
Refcounter<T>::Refcounter (const Refcounter &rhs)
  : ptr_ (rhs.ptr_) {
  increment ();
}

  /// Dtor will delete pointer if refcount becomes 0
template <typename T>
Refcounter<T>::~Refcounter () {
  decrement ();
  // std::cout<<"Refcounter::~Refcounter()"<<this<<" "<<std::endl;
}

/// assignment operator for times when you don't want the reference
/// increased for incoming ptr.
template <typename T>
Refcounter<T> &
Refcounter<T>::operator= (T *ptr) {
  decrement ();
  ptr_ = new Shim (ptr);
  return *this;
}

  /// assignment operator
template <typename T>
Refcounter<T> &
Refcounter<T>::operator= (const Refcounter& rhs) {
  if (this != &rhs) {
    decrement();
    ptr_ = rhs.ptr_;
    increment();
  }
  return *this;
}

/// get the underlying pointer
template <typename T>
T * 
Refcounter<T>::get_ptr () {
  return ptr_->t_;
}

/// get the underlying pointer
template <typename T>
Component_Node *
Refcounter<T>::get_ptr () const {
  return ptr_->t_;
}

/// dereference operator
template <typename T>
T & 
Refcounter<T>::operator* () {
  return *ptr_->t_;
}

/// dereference operator
template <typename T>
const 
T &
Refcounter<T>::operator* () const {
  return *ptr_->t_;
}

/// mimic pointer dereferencing
template <typename T>
T *
Refcounter<T>::operator-> () {
  return ptr_->t_;
}

/// mimic pointer dereferencing
template <typename T>
const T *
Refcounter<T>::operator-> () const {
  return ptr_->t_;
}

/// implementation of the increment operation
template <typename T>
void 
Refcounter<T>::increment () {
  if (ptr_) {
      ++ptr_->refcount_;
      // std::cout << "Refcounter::increment(): " <<this<<" "<< ptr_->refcount_ << std::endl;
  }

}

/// implementation of the decrement operation
template <typename T>
void 
Refcounter<T>::decrement () {
  if (ptr_ == nullptr){
    // std::cout<<"ptr_ is NULL, decrement() does not do anything"<<this<<" "<<std::endl;
  }
  else {
    // std::cout<<"Refcounter::decrement(): before decrement "<<this<<" "<<ptr_->refcount_<<std::endl;
    --ptr_->refcount_;
    // std::cout<<"Refcounter::decrement(a): "<<this<<" "<<ptr_->refcount_<<std::endl;
    if (ptr_->refcount_ <= 0) {
      // std::cout<<"Refcounter::decrement(b) deletion with "<<this<<" "<<ptr_->refcount_<<std::endl;
      delete ptr_;
      ptr_ = nullptr;
    }
  }
}

template <typename T>
Refcounter<T>::Shim::Shim (T *t,
                           bool omit_delete)
  : t_ (t),
    refcount_ (1),
    omit_delete_ (omit_delete) {
  // std::cout<<"Shim::Shim constructor: "<<this<<" "<<refcount_<<std::endl;
}

template <typename T>
Refcounter<T>::Shim::~Shim () {
  // std::cout<<"Shim::~Shim destructor: "<<this<<" "<<refcount_<<std::endl;
  if (!omit_delete_)
    delete t_; 
}

#endif /* _REFCOUNTER_CPP_ */
